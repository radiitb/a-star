#include <iostream>
#include <set>
#include <limits>
#include <vector>
#include <unordered_map>
#include <algorithm>
#include <cmath>
using namespace std;

#define INT_MAX numeric_limits<int>::max()

struct state
{	
	int id;
	state* parent;
	int parentArray[3][3];
	int cost = INT_MAX;
	int g;
	int childCount = 0;
	bool startNode = false;
	int tiles[3][3];
	bool isDoNOT = false;




	inline bool operator<(const state& a) const {
		if (this->cost <  a.cost){
			//cout<<"\t This cost: "<<this->cost<<" "<<a.cost<<endl;
			return true;
		}
		if (this->cost == a.cost && this->getId() < a.getId()){
			return true;
		}
		return false;
	}

	inline bool operator==(const state& a) const{
		if (a.getId() == this->getId() && a.cost == this->cost){
			return true;
		}
		else return false;
	}

	int getId() const{
		if (isDoNOT) return -1;
		int temp = 0;
		int count = 1;
		for (int i=0; i< 3; i++){
			for (int j=0; j< 3; j++){
				temp = temp * 10 + tiles[i][j];
			}
		}
		//id = temp;
		return temp;
	}

	bool isGoalNode(){
		bool goal = true;
		int count = 1;
		for (int i=0; i< 3; i++){
			for (int j=0; j< 3; j++){
				if (tiles[i][j] != (count % 9)) goal = false;
				count++;
			}
		}
		return goal;
	}

	vector<state> getNextStates(){

		childCount = 0;

		vector<state> allStates;
		int zeroI;
		int zeroJ;
		for (int i=0; i< 3; i++){
			for (int j=0; j< 3; j++){
				if (tiles[i][j] == 0){
					zeroI= i;
					zeroJ = j;
					break;
				}
			}
		}

		if (zeroI == 0 || zeroI == 1){
			state temp = *this;
			temp.startNode = false;
			temp.cost = INT_MAX;

			swap(temp.tiles[zeroI][zeroJ], temp.tiles[zeroI + 1][zeroJ]);
			allStates.push_back(temp);
			childCount ++;
		}

		if (zeroI == 1 || zeroI == 2){
			state temp = *this;
			temp.startNode = false;
			temp.cost = INT_MAX;

			swap(temp.tiles[zeroI][zeroJ], temp.tiles[zeroI - 1][zeroJ]);
			allStates.push_back(temp);
			childCount ++;
		}

		if (zeroJ == 0 || zeroJ == 1){
			state temp = *this;
			temp.startNode = false;
			temp.cost = INT_MAX;

			swap(temp.tiles[zeroI][zeroJ], temp.tiles[zeroI][zeroJ + 1]);
			allStates.push_back(temp);
			childCount ++;
		}

		if (zeroJ == 1 || zeroJ == 2){
			state temp = *this;
			temp.startNode = false;
			temp.cost = INT_MAX;

			swap(temp.tiles[zeroI][zeroJ], temp.tiles[zeroI][zeroJ - 1]);
			allStates.push_back(temp);
			childCount ++;
		}

		return allStates;

		
	}

	int getChildCount(){
		return childCount;
	}

	int getHeuristicValue(){
		//return 0;
		//SEcond heuristic
		
		/*
		number of displace tiles
		int count = 0;
		for (int i=0; i< 3; i++){
			for (int j=0; j< 3; j++){
				if ( ((i*3+j+1) % 9) != tiles[i][j]){
					count ++;
				}
			}
		}
		//return count;
		*/
		
		
		int manhattan = 0;
		for (int i=0; i<3; i++){
			for (int j=0; j< 3; j++){
				int currentVal = tiles[i][j];
				if (currentVal == 0){
					manhattan = manhattan + abs(2-i) + abs(2-j);
				}
				else{
					int tempVal = currentVal - 1;
					int cordi = tempVal/3;
					int cordj = tempVal % 3;
					manhattan = manhattan + abs(cordj - j) + abs(cordi - i);
				}
			}
		}
		return manhattan;
		

		//euler distance
		/*int euler = 0;
		for (int i=0; i<3; i++){
			for (int j=0; j< 3; j++){
				int currentVal = tiles[i][j];
				if (currentVal == 0){
					euler = euler + (int)sqrt(pow(2-i,2) + pow(2-j,2));
				}
				else{
					int tempVal = currentVal - 1;
					int cordi = tempVal/3;
					int cordj = tempVal % 3;
					euler = euler + (int)sqrt(pow(cordi-i,2) + pow(cordj-j,2));
				}
			}
		}
		return euler;
*/
	}

	int getEdgeCost(const state s){
		return 1;
	}


};



int main(){

	//DataType have to be changed from Nodes to Nodes
	unordered_map <int, int> includedNodes;

	state start, startParent;
	startParent.isDoNOT = true;
	start.parent = &startParent;
	start.cost = 0;
	start.g = 0;

	start.startNode = true;

	int tempTiles[3][3] = {{4,3,6}, {2,1,8}, {7,0,5}};
	//int tempTiles[3][3] = {{1,2,3}, {4,5,6}, {0,7,8}};
	for (int i=0; i< 3; i++){
		for (int j=0; j<3; j++){
			start.tiles[i][j] = tempTiles[i][j];
		}
	}


	set<state> openList;
	set<state> closeList;
	openList.insert(start);
	int nodeExpanded = 0;

	while(true){
		nodeExpanded ++;
		//cout<<"inside while"<<endl;
		state topNode = *(openList.begin());
		//cout<<topNode.getId()<<" "<<topNode.cost<<" "<<topNode.getHeuristicValue()<<endl;
		//for (int i=0; i< 100000000; i++){};
		if (topNode.isGoalNode()){
			cout<<"Goal F value: "<<topNode.g<<" Expanded Node: "<<nodeExpanded<<endl;
			
			while(topNode.getId() != -1){
				cout<<topNode.getId()<<endl;
				state temp;
				for (int i=0; i< 3; i++){
					for (int j=0; j< 3; j++){
						temp.tiles[i][j] = topNode.parentArray[i][j];
					}
				}
				int cost = includedNodes[temp.getId()];
				temp.cost = cost;
				set<state>::iterator it1 = openList.find(temp);

				if (it1 != openList.end()){
					state pstate = *it1;
					topNode = pstate;
				}
				else {
					set<state>::iterator it2 = closeList.find(temp);
					state pstate = *it2;
					topNode = pstate;
				}
			}

			break;
		}
		includedNodes.insert(make_pair(topNode.getId(), topNode.cost));

		vector<state> childs = topNode.getNextStates();
		int childCount = topNode.getChildCount();
		closeList.insert(topNode);
		openList.erase(openList.begin());

		for (int i=0; i < childCount; i++){
			state current = childs[i];

			if (current.startNode || current.getId() == topNode.parent->getId()){
				continue;
			}
			int newCost = topNode.cost + current.getEdgeCost(topNode) + current.getHeuristicValue(); // parent cost + edge cose + heuristic value
			
			unordered_map<int, int>::iterator foundIt = includedNodes.find(current.getId());
			if (foundIt != includedNodes.end()){
				current.cost = foundIt->second;
			}

			set<state>::iterator it = openList.find(current);
			if (it == openList.end()){
				set<state>::iterator closeIt = closeList.find(current);
				if (closeIt == closeList.end()){
					current.parent = &topNode;
					for (int i=0; i < 3; i++){
						for (int j=0; j< 3; j++){
							current.parentArray[i][j] = topNode.tiles[i][j];
						}
					}
					current.cost = newCost;
					current.g = topNode.g + current.getEdgeCost(topNode);
					openList.insert(current);
				}
				else{
					if (newCost < current.cost){
						closeList.erase(closeIt);

						current.parent = &topNode;
						for (int i=0; i < 3; i++){
							for (int j=0; j< 3; j++){
								current.parentArray[i][j] = topNode.tiles[i][j];
							}
						}
						current.cost = newCost;
						current.g = topNode.g + current.getEdgeCost(topNode);
						openList.insert(current);
					}
				}
				includedNodes.insert(make_pair(current.getId(), current.cost));
				continue;
			}
			if (newCost >= current.cost){
				continue;
			}
			openList.erase(it);
			current.parent = &topNode;
			for (int i=0; i < 3; i++){
				for (int j=0; j< 3; j++){
					current.parentArray[i][j] = topNode.tiles[i][j];
				}
			}
			current.cost = newCost;
			current.g = topNode.g + current.getEdgeCost(topNode);
			openList.insert(current);
			includedNodes.insert(make_pair(current.getId(), current.cost));
		}
	}

	return 0;

}